<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Mercados_mod extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
    function crearPersona($data){
        $this->db->insert('col.persona', $data);
        $id = $this->db->insert_id();
        $data = array(
            'fk_cabeza'     => $id,
            'fk_integrante' => $id,
            'parentesco'    => 'CABEZA'
        );
        $this->db->insert('col.persona_nucleo', $data);  
        return $id;      
    }
    function getPersonaMovil($movil){
        return $this->db->select('id_per, fk_ciu, nombres, apellidos, email, direccion')
                        ->from('col.persona')
                        ->where('movil', $movil)
                        ->get()->row();
    }
    function setSolicitudRecogida($idPersona){
        $data = array(
            'fk_per' => $idPersona
        );
        return $this->db->insert('col.mercado', $data);
    }
    function recoleccion($movil){
        return $this->db->select('id_per, fk_ciu, nombres, apellidos, email, direccion')
                        ->from('col.persona')
                        ->where('movil', $movil)
                        ->get()->row();
    }
}