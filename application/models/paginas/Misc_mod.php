<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Misc_mod extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	function getMunicipios($dpto){
		return $this->db->select('ciudades')
						->from('col.departamento')
						->where('divipola', $dpto)
						->get()->row()->ciudades;
	}
}