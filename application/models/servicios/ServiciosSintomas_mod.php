<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	class ServiciosSintomas_mod extends CI_Model {
		function __construct(){
			parent::__construct();
		}
		function cargarDolencias($esquema){
			return $this->db->get($esquema.'.dolencia')->result();
		}
		function cargarSintomas($dolencia, $esquema){
			$this->db->where('fk_dol', $dolencia);
			return $this->db->get($esquema.'.sintoma')->result();
		}
		function guardarSintomasPersona($data, $esquema){
			$this->db->insert_batch($esquema.'.persona_sintoma', $data);
			$id = $this->db->insert_id();
			$data = array(
				'fk_ps'  => $data->fk_per, 
				'fk_est' => '1' 
			);
			return $this->db->insert($esquema.'.persona_sintoma_estado', $data);
		}
	}