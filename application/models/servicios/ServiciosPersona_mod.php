<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	class ServiciosPersona_mod extends CI_Model {
		function __construct(){
			parent::__construct();
		}
		function login($imei, $esquema){
			return $this->db->select('id_per, fk_ciu, movil, nombres, apellidos, alias, telefono, direccion, direccion2, email, fecha_nacimiento, tipo_sangre, rh, sisben, eps')
							->from($esquema.'.persona')
							->where('imei', $imei)
							->get()->row();
		}
		function nuevaPersona($data, $codigo, $esquema){
			if($this->db->insert($esquema.'.ztmp_persona', $data)){
				$id = $this->db->insert_id();
				$data = array('codigo' => $codigo);
				$this->db->where('id_pertmp', $id);
				return $this->db->update($esquema.'.ztmp_persona', $data);
			}else{
				return false;
			}
		}
		function guardarPersona($data, $esquema, $id_cabeza = null, $parentesco = null){
			$this->db->insert($esquema.'.persona', $data);
			if($id_cabeza == null){
				$id_cabeza = $this->db->insert_id();
				$parentesco = 'CABEZA';
			}
			$data = array(
				'id_cabeza' => $id_cabeza, 
				'id_integrante' => $id_cabeza, 
				'parentesco' => $parentesco
			);
			$this->db->insert($esquema.'.persona_nucleo', $data);
		}
		function verificarMovil($data, $esquema){
			$this->db->where($data);
			return $this->db->count_all($esquema.'.persona');
		}
		function validarPersona($codigo, $movil, $esquema){
			$where = array(
				'codigo' => $codigo,
				'movil'  =>$movil
			);
			$data = $this->db->select('fk_ciu, movil, nombres, apellidos, alias, telefono, direccion, direccion2, email, fecha_nacimiento, tipo_sangre, rh, sisben, eps, localizacion')
								->from($esquema.'.ztmp_persona')
								->where($where)
								->get();
			if($data->num_rows()>0){
				$this->db->where($where);
				$this->db->delete($esquema.'.ztmp_persona');
				return $this->db->insert($esquema.'.persona', $data->row());
			}
			return false;
		}
		function cargarPersonasMovil($movil, $esquema){
			$this->db->where('movil', $movil);
			return $this->db->get($esquema.'.persona')->result();
		}
		function cargarNucleo($id_cabeza, $esquema){
			return $this->db->select('p.id_per, p.fk_ciu, p.movil, p.nombres, p.apellidos, p.alias, p.telefono, p.direccion, p.direccion2, p.email, p.fecha_nacimiento, p.tipo_sangre, p.rh, p.sisben, p.eps, p.localizacion, pn.parentesco')
							->from($esquema.'.persona p')
							->join($esquema.'.persona_nucleo pn', 'p.id_per=pn.fk_integrante', 'left')
							->where('fk_cabeza', $id_cabeza)
							->order_by('id_per, parentesco')
							->get()->result();
		}
		function cargarCiudades($esquema){
			return $this->db->select('departamento, divipola, ciudades')
							->from($esquema.'.departamento')
							->get()->result();
		}
	}
?>