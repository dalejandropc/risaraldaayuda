<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * MLogin
 * @uses CI_Model
 * @package Chami
 * @version $1.0$
 * @author DAPC
 *
 * Description: Modelo de LOgin, obtiene la data de usuario y menú para el usuario (El menú aún no ha sido necesario usarlo, el menú actual es muy chico).
 */

class Login_mod extends CI_Model{

	public function verifyUser($mail, $password, $ip) {
		return $this->db->query("SELECT misc.loguear('$mail', '$password', '$ip')")->row()->loguear;
	}

	public function getUser($id) {
		return $this->db->select("id_ctr, usuario, perfil")
						->from('col.control')
						->where('id_ctr', $id)
						->get()->row();
	}
}