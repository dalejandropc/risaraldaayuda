<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*Rutas de servicios Personas*/
$route['ingresoSrv'] = 'servicios/ServiciosPersona/ingreso';
$route['ciudadesSrv'] = 'servicios/ServiciosPersona/cargarCiudades';
$route['personaNuevaSrv'] = 'servicios/ServiciosPersona/nuevaPersona';
$route['validarPersonaSrv'] = 'servicios/ServiciosPersona/validarPersona';
$route['guardarPersonaSrv'] = 'servicios/ServiciosPersona/guardarPersonaPersona';
$route['cargarPersonasSrv'] = 'servicios/ServiciosPersona/cargarPersonasMovil';
$route['cargarNucleoSrv'] = 'servicios/ServiciosPersona/cargarNucleo';

/*Rutas de servicios Sintomas*/
$route['cargarPatologiasSrv'] = 'servicios/ServiciosSintomas/cargarDolencias';
$route['cargarSintomasSrv'] = 'servicios/ServiciosSintomas/cargarSintomas';
$route['guardarSintomasSrv'] = 'servicios/ServiciosPersona/guardarSintomasPersona';

/*Aplicaciones*/
$route['ingresar'] = 'misc/login';
$route['verRecoleccion'] = 'paginas/Administrar';

