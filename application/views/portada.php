
<body id="page-top">

	<!-- Navigation -->
	<a class="menu-toggle rounded" href="#">
		<i class="fas fa-bars"></i>
	</a>
	<nav id="sidebar-wrapper">
		<ul class="sidebar-nav">
			<li class="sidebar-brand">
				<a class="js-scroll-trigger" href="#page-top">Risaralda Unida</a>
			</li>
			<li class="sidebar-nav-item">
				<a class="js-scroll-trigger" href="#page-top">Inicio</a>
			</li>
			<li class="sidebar-nav-item">
				<a class="js-scroll-trigger" href="#about">Somos</a>
			</li>
			<li class="sidebar-nav-item">
				<a class="js-scroll-trigger" href="#services">Proyectos</a>
			</li>
			<li class="sidebar-nav-item">
				<a class="js-scroll-trigger" href="#help">Ayudanos</a>
			</li>
			<li class="sidebar-nav-item">
				<a class="js-scroll-trigger" href="#portfolio">Imagenes</a>
			</li>
			<!--li class="sidebar-nav-item">
				<a class="js-scroll-trigger" href="#contact">Contactanos</a>
			</li-->
		</ul>
	</nav>

	<!-- Header -->
	<header class="masthead d-flex">
		<div class="container text-center my-auto">
			<h1 class="mb-1 text-info">Risaralda Unida</h1>
			<h3 class="mb-5">
				<em>Unidos somos mas...</em>
			</h3>
			<a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Somos</a>
		</div>
		<div class="overlay"></div>
	</header>
	<!-- About -->
	<section class="content-section bg-light" id="about">
		<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">

			<div class="row">
				<?php 
				$i = 1;
				foreach ($team as $key => $value) { ?>
					<div class="col-sm-4 col-md-3 col-lg-2 text-center">
						<div class="thumbnail">
							<img src="<?=base_url('assets/imagenes/team/team-'.$i.'.jpg')?>" class="rounded-circle" alt="<?=$key?>" style="max-width: 150px">
							<div class="caption">
								<h5><?=$key?></h5>
								<?=$value?>
							</div>
						</div>
					</div>
				<?php $i++; } ?>
			</div>
		</div>
	</section>



  <!-- Services -->
  <section class="content-section bg-primary text-white text-center" id="services">
    <div class="container">
      <div class="content-section-heading">
        <h3 class="text-secondary mb-0">Proyectos</h3>
        <h2 class="mb-5">Actualmente nuestra investigación se centra en</h2>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="fas fa-heart"></i>
          </span>
          <h4>
            <strong>Respiradores</strong>
          </h4>
          <p class="text-faded mb-0">Buscamos replicar un proyecto de ayuda para la respiración artificial económico y práctico</p>
        </div>
        <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="fas fa-check-double"></i>
          </span>
          <h4>
            <strong>Mascarillas</strong>
          </h4>
          <p class="text-faded mb-0">Utilizando impresión 3D, investigamos como generar mascarillas duraderas y de bajo coste.</p>
        </div>
        <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="icon-like"></i>
          </span>
          <h4>
            <strong>Aspersión</strong>
          </h4>
          <p class="text-faded mb-0">Nos encontramos investigando métodos de aspersión de liquidos sencillos para la ciudad y nuestros hogares.</p>
        </div>
        <div class="col-lg-3 col-md-6">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="icon-screen-smartphone"></i>
          </span>
          <h4>
            <strong>TiC en tu ayuda</strong>
          </h4>
          <p class="text-faded mb-0">Se encuentra en desarrollo nuestra primera aplicación móvil para ayudar en la comunicación paciente estamentos de salud, desde la comodidad de su casa.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- Callout -->
  <section class="callout">
    <div class="container text-center">
      <form id="help">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<h3 class="card-title">Unete a nuestra campaña!!!</h3>
							<p class="card-text">Al hacer clic en el botón esta de acuerdo que uno de nuestros funcionarios se acerque a su vivienda para recoger un mercado para los mas necesitados........</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-lg-6">
					<div class="form-group">
						<label for="movil">Telefono</label>
						<input type="phone" class="form-control" id="movil" aria-describedby="movilHelp" placeholder="Ingrese Telefono">
						<input type="hidden" id="idPersona">
						<small id="movilHelp" class="form-text text-muted">Digite su movil por favor.</small>
					</div>
					<div class="form-group">
						<label for="nombres">Nombres</label>
						<input type="text" class="form-control a" id="nombres" aria-describedby="nombreHelp" placeholder="Ingrese nombres" readonly="true">
						<small id="nombreHelp" class="form-text text-muted">Digite sus nombres por favor.</small>
					</div>
					<div class="form-group">
						<label for="apellidos">Apellidos</label>
						<input type="text" class="form-control a" id="apellidos" aria-describedby="apellidosHelp" placeholder="Ingrese Apellidos" readonly="true">
						<small id="apellidosHelp" class="form-text text-muted">Digite sus apellidos por favor.</small>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control a" id="email" aria-describedby="emailHelp" placeholder="Ingrese Telefono" readonly="true">
						<small id="emailHelp" class="form-text text-muted">Digite su email por favor.</small>
					</div>
				</div>
				<div class="col-12 col-lg-6">
					<div class="form-group">
						<label for="direccion">Dirección</label>
						<input type="address" class="form-control a" id="direccion" aria-describedby="direccionHelp" placeholder="Ingrese Dirección" readonly="true">
						<small id="direccionHelp" class="form-text text-muted">Digite sus dirección por favor.</small>
					</div>
					<div class="form-group">
						<label for="dpto">Departamento</label>
						<select class="form-control" id="dpto" aria-describedby="dptoHelp" placeholder="Ingrese Diorección">
							<?php $this->load->view('misc/departamentos')?>
						</select>
						<small id="dptoHelp" class="form-text text-muted">Seleccione Departamento.</small>
					</div>
					<div class="form-group">
						<label for="ciudad">Ciudad</label>
						<select class="form-control" id="ciudad" aria-describedby="ciudadHelp" placeholder="Ingrese Diorección">
						</select>
						<small id="ciudadHelp" class="form-text text-muted">Seleccione Ciudad.</small>
					</div>
					<button type="button" class="btn btn-primary" id="guardar"><i class="fas fa-save"></i> Guardar</button>
				</div>
			</div>
			
		</form>
    </div>
  </section>

	<!-- Portfolio AQUI SE COLOCARAN LAS IMAGENES DE AVANCES-->
	<section class="content-section" id="portfolio">
		<div class="container">
			<div class="content-section-heading text-center">
				<h3 class="text-secondary mb-0">Portafolio</h3>
				<h2 class="mb-5">Proyectos Actuales</h2>
			</div>
			<div class="row no-gutters">
				<div class="col-lg-6">
					<a class="portfolio-item" href="#">
						<span class="caption">
							<span class="caption-content">
								<h2>Modelo 1</h2>
								<p class="mb-0">Primer modelo digital para la construcción del respirador</p>
							</span>
						</span>
						<img class="img-fluid" src="<?=base_url('assets/plantilla/')?>img/portfolio-1.jpg" alt="">
					</a>
				</div>
				<div class="col-lg-6">
					<a class="portfolio-item" href="#">
						<span class="caption">
							<span class="caption-content">
								<h2>Modelo 1</h2>
								<p class="mb-0">Primer modelo digital para la construcción del respirador</p>
							</span>
						</span>
						<img class="img-fluid" src="<?=base_url('assets/plantilla/')?>img/portfolio-2.jpg" alt="">
					</a>
				</div>
				<div class="col-lg-6">
					<a class="portfolio-item" href="#">
						<span class="caption">
							<span class="caption-content">
								<h2>Avances Prototipo</h2>
								<p class="mb-0">A punto de lograr nuestro primer objetivo, versión 1.0 del respirador!!!.</p>
							</span>
						</span>
						<img class="img-fluid" src="<?=base_url('assets/plantilla/')?>img/portfolio-5.jpg" alt="">
					</a>
				</div>
				<div class="col-lg-6">
					<a class="portfolio-item" href="#">
						<span class="caption">
							<span class="caption-content">
								<h2>Prototipo Aplicación</h2>
								<p class="mb-0">Primer prototipo para la aplicación móvil de comunicación paciente- funcionarios de la salud.</p>
							</span>
						</span>
						<img class="img-fluid" src="<?=base_url('assets/plantilla/')?>img/portfolio-3.jpg" alt="">
					</a>
				</div>
				<div class="col-lg-6">
					<a class="portfolio-item" href="#">
						<span class="caption">
							<span class="caption-content">
								<h2>Prototipo Máscara</h2>
								<p class="mb-0">Primer máscara construida mediante impresión 3D.</p>
							</span>
						</span>
						<img class="img-fluid" src="<?=base_url('assets/plantilla/')?>img/portfolio-4.jpg" alt="">
					</a>
				</div>
			</div>
		</div>
	</section>

	<!-- Call to Action LO COLOCO POR SI LO NECESITAMOS MAS ADELANTE... (Recoger fondos o algo asi...)-->
	<!--section class="content-section bg-primary text-white">
		<div class="container text-center">
			<h2 class="mb-4">The buttons below are impossible to resist...</h2>
			<a href="#" class="btn btn-xl btn-light mr-4">Click Me!</a>
			<a href="#" class="btn btn-xl btn-dark">Look at Me!</a>
		</div>
	</section-->

	<!-- Footer -->
	<footer class="footer text-center">
		<div class="container">
			<ul class="list-inline mb-5">
				<li class="list-inline-item">
					<a class="social-link rounded-circle text-white mr-3" href="#">
						<i class="icon-social-facebook"></i>
					</a>
				</li>
				<li class="list-inline-item">
					<a class="social-link rounded-circle text-white mr-3" href="#">
						<i class="icon-social-twitter"></i>
					</a>
				</li>
				<li class="list-inline-item">
					<a class="social-link rounded-circle text-white" href="https://wa.me/573215492872" target="_blank">
						<i class="fab fa-whatsapp"></i>
					</a>
				</li>
			</ul>
			<p class="text-muted small mb-0">Copyright &copy; Your Website 2019</p>
		</div>
	</footer>

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
	<i class="fas fa-angle-up"></i>
	</a>

	<!-- Bootstrap core JavaScript -->
	<script src="<?=base_url('assets/plantilla/')?>vendor/jquery/jquery.min.js"></script>
	<script src="<?=base_url('assets/plantilla/')?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="<?=base_url('assets/plantilla/')?>vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for this template -->
	<script src="<?=base_url('assets/plantilla/')?>js/stylish-portfolio.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script>var baseUrl = '<?=base_url()?>'</script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			cargarCiudad();

			var solicitarRecogida = false;

			$("#dpto").change(function(){
    			cargarCiudad();
			});
			$("#guardar").click(function(){
				data = {
					fk_ciu    : $("#ciudad").val(),
					nombres   : $("#nombres").val(),
					apellidos : $("#apellidos").val(),
					movil     : $("#movil").val(),
					email     : $("#email").val(),
					direccion : $("#direccion").val(),
				};
				data.nombres = data.nombres.toUpperCase();
				data.apellidos = data.apellidos.toUpperCase();
				data.direccion = data.direccion.toUpperCase();

				var idPersona = $("#idPersona").val();
				data = JSON.stringify(data);
				if(data){
					$.ajax({
						type : 'post',
						url  : baseUrl + ('/paginas/Paginas/setSolicitudRecogida'),
						data : {data : data, idPersona : idPersona}
					}).done(function(data){
						if(data){
							alertas('success', 'Solicitud de recogida Guardada.')
						}else{	
							alertas('warning', 'No fué posible guardar su solicitud.')						
						}
					}).fail(function(e){
						alertas('warning', 'No fué posible guardar su solicitud.')
					});
				}
			});
			$("#movil").change(function(){
				var movil = $("#movil").val();
				solicitarRecogida = false;
				if(movil){
					$.ajax({
						type : 'post',
						url  : baseUrl + ('/paginas/Paginas/getPersonaMovil'),
						data : {movil : movil}
					}).done(function(data){
						json = JSON.parse(data)
						$(".a").attr('readonly', true);
						$(".a").val('');
						if(json){
							$("#idPersona").val(json.id_per);
							$("#nombres").val(json.nombres);
							$("#apellidos").val(json.apellidos);
							$("#email").val(json.email);
							$("#direccion").val(json.direccion);
							$("#guardar").html('Solicitar Recogida');
							$("#guardar").removeClass('btn-primary');
							$("#guardar").addClass('btn-success');
							solicitarRecogida = true;
						}
						else{
							$(".form-control").attr('readonly', false);
							$("#guardar").html('Guardar y Solicitar Recogida');
							$("#guardar").removeClass('btn-success');
							$("#guardar").addClass('btn-primary');
							solicitarRecogida = false;
						}
					});
				}
			});
		});
		function alertas(icono, mensaje){
			Swal.fire({
				position: 'top-end',
				icon: icono,
				title: mensaje,
				showConfirmButton: false,
				timer: 1500
			})
		}
		function cargarCiudad(){
			$("#ciudad option").remove();
			var dpto = $("#dpto option:selected").val();
			if(dpto){
				$.ajax({
					type : 'post',
					url  : baseUrl + ('Welcome/getCiudades'),
					data : {dpto : dpto}
				}).done(function(data){

        			json= JSON.parse(data);
        			json= JSON.parse(json);
					$.each(json, function(key, value){
			            $("#ciudad").append('<option value="'+key+'">'+value+'</option>')
			        });
				});
			}
		}
	</script>

	</body>

</html>