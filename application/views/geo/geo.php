<! DOCTYPE html>
  <head>
    <meta name = "viewport" content = "initial-scale = 1.0, escalable por el usuario = no" />
    <meta http-equiv = "content-type" content = "text / html; charset = UTF-8" />
    <title> Uso de MySQL y PHP con Google Maps </title>
    <style>
      / * Siempre establece la altura del mapa explícitamente para definir el tamaño del div
       * elemento que contiene el mapa. * /
      #mapa {
        altura: 100%;
      }
      / * Opcional: hace que la página de muestra llene la ventana. * /
      html, cuerpo {
        altura: 100%;
        margen: 0;
        relleno: 0;
      }
    </style>
  </head>

<html>
  <cuerpo>
    <div id = "map"> </div>

    <script>
      var customLabel = {
        restaurante: {
          etiqueta: 'R'
        },
        bar: {
          etiqueta: 'B'
        }
      };

        función initMap () {
        mapa var = nuevo google.maps.Map (document.getElementById ('mapa'), {
          centro: nuevo google.maps.LatLng (-33.863276, 151.207977),
          zoom: 12
        });
        var infoWindow = new google.maps.InfoWindow;

          // Cambia esto dependiendo del nombre de tu archivo PHP o XML
          downloadUrl ('https://storage.googleapis.com/mapsdevsite/json/mapmarkers2.xml', function (data) {
            var xml = data.responseXML;
            marcadores var = xml.documentElement.getElementsByTagName ('marcador');
            Array.prototype.forEach.call (marcadores, función (markerElem) {
              var id = markerElem.getAttribute ('id');
              nombre var = markerElem.getAttribute ('nombre');
              var dirección = markerElem.getAttribute ('dirección');
              var type = markerElem.getAttribute ('type');
              punto var = nuevo google.maps.LatLng (
                  parseFloat (markerElem.getAttribute ('lat')),
                  parseFloat (markerElem.getAttribute ('lng')));

              var infowincontent = document.createElement ('div');
              var fuerte = document.createElement ('fuerte');
              strong.textContent = nombre
              infowincontent.appendChild (fuerte);
              infowincontent.appendChild (document.createElement ('br'));

              var text = document.createElement ('texto');
              text.textContent = dirección
              infowincontent.appendChild (texto);
              icono var = etiqueta personalizada [tipo] || {};
              marcador var = nuevo google.maps.Marker ({
                mapa: mapa,
                posición: punto,
                etiqueta: icon.label
              });
              marker.addListener ('click', function () {
                infoWindow.setContent (infowincontent);
                infoWindow.open (mapa, marcador);
              });
            });
          });
        }



      función downloadUrl (url, devolución de llamada) {
        var request = window.ActiveXObject?
            nuevo ActiveXObject ('Microsoft.XMLHTTP'):
            nuevo XMLHttpRequest;

        request.onreadystatechange = function () {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            devolución de llamada (request, request.status);
          }
        };

        request.open ('GET', url, true);
        request.send (nulo);
      }

      función doNothing () {}
    </script>
    <script async diferir
    src = "https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY& callback = initMap ">
    </script>
  </body>
</html>