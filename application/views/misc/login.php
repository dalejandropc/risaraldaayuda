	<div class="wrapper">
		<br><br><br>
		<div class ="row">
			<div class="col-5">
			</div>
			<div class="col-12 col-lg-2">
				<!-- START card-->
				<div class ="card">
					<div class="card-header text-center bg-success">

					</div>
					<div class="card-body">
						<p class ="text-center py-2">Ingresar</p>
						<form method ="post" id="formLogin" class="mb-3">
							<div class="form-group">
								<div class="input-group with-focus">
									<input class="form-control border-right-0" name="mail" id="mail" type="text" placeholder="Digite su usuario" autocomplete="off" required >
									<div class="input-group-append">
										<span class ="input-group-text text-muted bg-transparent border-left-0">
											<i class="fas fa-user-check"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group with-focus">
									<input class="form-control border-right-0" name="password" id="password" type="password" placeholder="Digite su password" required>
									<div class="input-group-append">
										<span class="input-group-text text-muted bg-transparent border-left-0">
											<i class="fas fa-user-lock"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="clearfix">
								<div class="text-center">
									<a class="text-muted" href="<?=base_url()?>recovery">
										¿Olvidaste tu contraseña?
									</a>
								</div>
							</div>
							<button class ="btn btn-block btn-success mt-3" type="button" id="btnSession">
								Ingresar
							</button>
						</form>
					</div>
				</div>
				<!-- END card-->
				<div class="p-3 text-center">
					<p class="font-weight-bold" style="color: #030F44">
						Risaralda Ayuda<sup> <i class="fas fa-copyright"></i> </sup>
						Cree en Risaralda </i>
					</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>