<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name ="description" content="Apoya a Risaralda, apoyo, coronavirus, dona">
	<meta name ="keywords" content="taller, facturacion, inventarios, centro de servicio">
	<title>Risaralda Ayuda</title>
	<link rel="icon" type="image/png" href="<?=base_url()?>assets/imagenes/icon/icon.png" />

	<!-- Bootstrap Core CSS -->
	<link href="<?=base_url('assets/plantilla/')?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="<?=base_url('assets/plantilla/')?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/plantilla/')?>vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="<?=base_url('assets/plantilla/')?>css/stylish-portfolio.min.css" rel="stylesheet">
	<link href="<?=base_url('assets/')?>css/adicionales.css" rel="stylesheet">

	<!--link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/portada.css" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/a076d05399.js"></script-->
</head>