<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');

class Paginas extends CI_Controller {
	
	function setSolicitudRecogida(){
        $this->db->trans_start();
		$this->load->model('paginas/Mercados_mod');
		$idPersona = $this->input->post('idPersona');
		if($idPersona == ''){
			$data = json_decode($this->input->post('data'));
			$idPersona = $this->Mercados_mod->crearPersona($data);
		}
		$data = json_encode($this->Mercados_mod->setSolicitudRecogida($idPersona));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        	return null;
        die($data);
	}
	function getPersonaMovil(){
		$this->load->model('paginas/Mercados_mod');
		$movil = $this->input->post('movil');
		$data= $this->Mercados_mod->getPersonaMovil($movil);
		die(json_encode($data));
	}
}
