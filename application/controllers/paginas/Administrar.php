<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrar extends CI_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('paginas/Mercados_mod');
	}

	public function index() {
		$data = array(
			'listado' => $this->Mercados_mod->recoleccion(); 
		); 
		$this->load->view('misc/header');
		$this->load->view('misc/login');
	}
}