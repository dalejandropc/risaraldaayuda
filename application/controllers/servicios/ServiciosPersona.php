<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class ServiciosPersona extends RestController {
	function __construct(){
		parent::__construct();
		$this->load->model('servicios/ServiciosPersona_mod');
        $this->load->library('Misc');
		if(!$this->validar()){
            redirect(base_url(), 'refresh');
            return null;
		}
	}
	function validar(){
        $esquema = $this->input->get('esquema');
        $movil = $this->input->get('movil');
        $key = $this->input->get('key');
        if($esquema == '' || $movil == '' || $key == '')
            die(json_encode(array('Error' => '-999')));
        return $this->misc->validame($movil, $esquema, $key);
	}
    function ingreso_get(){
        $esquema = $this->input->get('esquema');
        $imei = $this->input->get('imei');
        if($esquema == '' || $imei == '')
            die(json_encode(array('Error' => '-999')));
        die(json_encode($this->ServiciosPersona_mod->login($imei, $esquema)));

    }
    function cargarCiudades_get(){
        $esquema = $this->input->get('esquema');
        die(json_encode($this->ServiciosPersona_mod->cargarCiudades($esquema)));
    }
    function nuevaPersona_get(){
        /*Para que el servico funcione mas agilmente no se hacen validaciones aquí, esto se deja al móvil, solo verificar el numero de movil*/
        $movil = $this->input->get('movil');
        $data = json_decode($this->input->get('data'));
        $esquema = $this->input->get('esquema');
        $where = array(
            'movil' => $movil
        );
        if($this->ServiciosPersona_mod->verificarMovil($where, $esquema)>0){
            $data = array(
                'Error'   => 'El movil existe en la base de datos'
            );
            die(json_encode($data));
        }
        $codigo = $this->misc->generarCodigo(5);
        $data = $this->ServiciosPersona_mod->nuevaPersona($data, $codigo, $esquema);
        $this->load->model('aws/Aws_mod');
        $this->Aws_mod->SendAwsSms($movil, 'Su código de validación es '. $codigo);
        $data = array(
            'data'   => $data
        );
        die(json_encode($data));
    }
    function guardarPersona_get(){
        /*Para crear otras personas amarradas al mismo móvil*/
        $movil = $this->input->get('movil');
        $data = json_decode($this->input->get('data'));
        $esquema = $this->input->get('esquema');
        $id_cabeza = $this->input->get('id_cabeza');
        $parentesco = $this->input->get('parentesco');
        $data = $this->ServiciosPersona_mod->guardarPersona($data, $esquema, $id_cabeza, $parentesco);
        $data = array(
            'data'   => $data
        );
        die(json_encode($data));
    }
    function validarPersona_get(){
        $movil = $this->input->get('movil');
        $codigo = $this->input->get('codigo');
        $esquema = $this->input->get('esquema');
        $data = $this->ServiciosPersona_mod->validarPersona($codigo, $movil, $esquema);
        die($data);
    }
    function cargarPersonasMovil_get(){
        $movil = $this->input->get('movil');
        $esquema = $this->input->get('esquema');
        die(json_encode($this->ServiciosPersona_mod->cargarPersonasMovil($movil, $esquema)));
    }
    function cargarNucleo_get(){
        $id_cabeza = $this->input->get('id');
        $esquema = $this->input->get('esquema');
        die(json_encode($this->ServiciosPersona_mod->cargarNucleo($id_cabeza, $esquema)));
    }
}