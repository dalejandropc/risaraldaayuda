<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class ServiciosSintomas extends RestController {
	function __construct(){
		parent::__construct();
		$this->load->model('servicios/ServiciosSintomas_mod');
        $this->load->library('Misc');
		if(!$this->validar()){
            redirect(base_url(), 'refresh');
            return null;
		}
	}
	function validar(){
        $esquema = $this->input->get('esquema');
        $movil = $this->input->get('movil');
        $key = $this->input->get('key');
        if($esquema == '' || $movil == '' || $key == '')
            die(json_encode(array('Error' => '-999')));
        return $this->misc->validame($movil, $esquema, $key);
	}
    function cargarDolencias_get(){
        $esquema = $this->input->get('esquema');
        $data = $this->ServiciosSintomas_mod->cargarDolencias($esquema);
        die(json_encode($data));
    }
    function cargarSintomas_get(){
        $esquema = $this->input->get('esquema');
        $dolencia = $this->input->get('dolencia');
        $data = $this->ServiciosSintomas_mod->cargarSintomas($dolencia, $esquema);
        die(json_encode($data));
    }
    function guardarSintomasPersona(){
        /*Para guardar los sintomas de una persona*/
        $data = json_decode($this->input->get('data'));
        $esquema = $this->input->get('esquema');
        $data = $this->ServiciosSintomas_mod->guardarSintomasPersona($data, $esquema);
        $data = array(
            'data'   => $data
        );
        die(json_encode($data));
    }
}