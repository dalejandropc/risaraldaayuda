<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');

class Welcome extends CI_Controller {
	public function index()
	{
		$team = array(
			'Mauricio León Iza.' => '<small>Médico y cirujano<br/>Profesor Fundación Universitaria de las Américas</small>', 
			'Diego Alejandro Pérez C.' => '<small>Ingeniero en Sistemas y Telecomunicaciones<br/>Especialista en Desarrollo de Software<br/>CEO y fundador Software Andes</small>',
			'Johan Riascos Segura' => '<small>Ingeniero Electrónico<br/>Warex Engineering</small>',
			'Samuel Alejandro Cortés Angel' => '<small>Ingeniero Mecánico<br/>Grupo Optimizando S.A.S.</small>',
			'Tania Latorre Muñoz.' => '<small>Administradora Ambiental UTP<br/>Consultora en Negocios Verdes <br/>Consultora en Bambú y Guadua</small>',
			'Diana Marcela Otálvaro López' => '<small>Ingeniera de Sistemas y Biomédica</small>',
			'Juan David Perez Jaramillo' => '<small>Ingeniero Mecatrónico<br/>Ingeniería 3D Pereira</small>',
			'Luis David Loaiza Gómez' => '<small>Ingeniero de Sistemas</small>',
			'Harold Herrera Ocampo' => '<small>Ingeniero en Mecatrónica</small>',
			'Jasmin Alexandra Cañaveral' => '<small>Ingeniera en Sistemas y Telecomunicaciones</small>',
			'César González Manrique' => '<small>Técnologo Profesional en Mecatrónica<br>Certificación CSWP-CSWA SolidWorks</small>',
			'Julián Londoño Giraldo' => '<small>Ingeniero Electrónico</small>',
			'Iván Zapata' => '<small>Ingeniero Electrónico<br>Warex Engineering S.A.S.</small>',
		);
		$data = array( 'team' => $team );
		$this->load->view('misc/header');
		$this->load->view('portada', $data);
	}
	function getCiudades(){
		//if($this->input->is_ajax_request()){
			$this->load->model('paginas/Misc_mod');
			$dpto = $this->input->post('dpto');
			die(json_encode($this->Misc_mod->getMunicipios($dpto)));
		//}
	}
}
