<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CLogin
 * @uses CI_Controller
 * @package Chami
 * @version $1.0$
 * @author DAPC
 *
 * Description: Controlador para loguear un usuario.
 */

class Login extends CI_Controller{

	public function __construct() {
		parent::__construct();
		$this->load->model('misc/Login_mod');
	}

	public function index() {
		$this->load->view('misc/header');
		$this->load->view('misc/login');
	}
	/**
	* Verifica la autenticación del usuario
	*
	* @access public
	* @param String mail, Email del usuario
	* @param String password, Password del usuario
	*/
	public function login(){
		$this->load->helper('realip');
		$ip         = getRealIP();
		$mail       = $this->input->post('mail');
		$returnData = 'error';
		$userid     = $this->Login_mod->verifyUser(strtolower($mail), $this->input->post('password'), $ip);
		if (!empty($userid)){
			$data = $this->Login_mod->getUser($userid);
			if (!empty($data) && $data->num_rows()==1){
				//Cargar las variables de sesion necesarias, nuevo método para usarlo en el caso de multiples resultados:
				$dataSession = $this->loadSession($data);
				//$this->getMenu();
				$returnData = array (
					'url'  => 'dashboard',
					'data' => $dataSession
				);
			}
		}
		echo json_encode($returnData);
	}

	/**
	* Setea las variables de sesión del usuario autenticado
	*
	* @access public
	*/
	public function loadSession($data){
		$dataSession = array();
		foreach ($data->result() as $row) {
			$menuArray = array(
				'usrId'      => $row->usr_id,
				'usrNick'    => $row->usr_nickname,
				'usrName'    => $row->usr_name,
				'usrSurname' => $row->usr_surname,
				'control'    => '1111111111111111111111111',
				'key'        => md5($row->usr_id.'230916'.$row->usr_surname)
			);
			$this->session->set_userdata($menuArray);
			$dataSession = $menuArray;
		}
		return $dataSession;
	}

	/**
	* Cierra sesión, destruye la sesión
	*
	* @access public
	*/
	public function logout(){
		session_destroy();
		redirect(base_url(), 'location');
	}
}
/* End of file CLogin.php */
/* Location: ./application/controllers/misc/CLogin.php */