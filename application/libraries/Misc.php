<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class misc
{
	/*Ejecutar siempre al principio de cada controlador de servicios FE...*/
    function validame($movil, $esquema, $key)
    {
        if(md5($movil.'70891638708'.$esquema) != $key){
            //Verifique Key, errada...
            die(json_encode(array('Error' => '-998')));
            return false;
        }
        return true;
    }
    function generarCodigo($longitud) {
        $key = '';
        $pattern = '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVXYZ';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }
}